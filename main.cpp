#include "Complex.h"
#include "DynamicArray.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>
#include <crtdbg.h>

using namespace std;

int main() {
	DynamicArray Array;

	ifstream valuesoffile;
	valuesoffile.open("complex_numbers.txt");
	float r, im;

	while (valuesoffile >> r >> im)
	{
		Array.addLast(Complex(r, im));
	}
	cout << Array;
	
	Complex sum;
	for (int i = 0; i < Array.getCap(); i++)
		sum = Array[i] + sum;

	cout << "The sum is: " << sum;
	_CrtDumpMemoryLeaks();
}
